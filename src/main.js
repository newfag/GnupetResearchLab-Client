import { createApp } from 'vue';
import HomeApp from './HomeApp.vue';
import GameApp from './GameApp.vue';
import 'normalize.css';
import 'remixicon/fonts/remixicon.css'

createApp(HomeApp).mount('#homeApp');
createApp(GameApp).mount('#gameApp');
